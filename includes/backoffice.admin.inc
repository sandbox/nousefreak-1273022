<?php
/**
 * @file
 * Admin page callbacks for the Backoffice module.
 */

function backoffice_admin_general_settings_form() {
  $form['backoffice_desktop_path'] = array(
    '#title' => t('Desktop path'),
    '#type' => 'textfield',
    '#description' => t('Enter the path for the backoffice desktop.'),
    '#default_value' => variable_get('backoffice_desktop_path', 'desktop'),
  );

  return system_settings_form($form);
}

function backoffice_admin_modules_form($form, $form_state) {
  $form = array();
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  $form['#tree'] = TRUE;

  $form['module_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Module title'),
    '#description' => t('Title for the module.'),
    '#required' => TRUE,
    '#maxlength' => 255,
  );

  $form['module_icon'] = array(
    '#type' => 'file',
    '#title' => t('Module icon'),
    '#description' => 'Icon displayed on the desktop.',
  );

  $form['submodule'] = array(
    '#type' => 'fieldset',
    '#title' => t('Submodules'),
    '#collapsible' => FALSE,
    '#prefix' => '<div id="backoffice-submodule-wrapper">',
    '#suffix' => '</div>',
  );

  $entities = array(0 => 'Please select an entity');
  foreach (node_type_get_names() as $machine_name => $content_type) {
    $entities[t('Content type')]['content_type||' . $machine_name] = $content_type;
  }
  foreach (array() as $machine_name => $taxonomy) {
    $entities[t('Taxonomy')]['taxonomy||' . $machine_name] = $taxonomy;
  }

  if (empty($form_state['form_structure'])) {
    $form_state['form_structure'] = array(1 => 1);
  }

  foreach ($form_state['form_structure'] as $submodules => $submodule_views) {
    $ix_submodules = $submodules - 1;
    $form['submodule'][$ix_submodules] = array(
      '#type' => 'item',
    );

    $form['submodule'][$ix_submodules]['smid'] = array(
      '#type' => 'hidden',
      '#title' => t('Submodule id'),
      '#required' => FALSE,
      '#value' => '',
    );

    $form['submodule'][$ix_submodules]['submodule_alias'] = array(
      '#type' => 'textfield',
      '#title' => t('Submodule alias'),
      '#description' => t('Overwrite the title of the submodule.'),
      '#required' => FALSE,
    );

    $form['submodule'][$ix_submodules]['item'] = array(
      '#type' => 'select',
      '#title' => t('Entity'),
      '#options' => $entities,
      '#default_value' => 0,
      '#description' => t('Select the basis for this module.'),
    );

    $form['submodule'][$ix_submodules]['view'] = array(
      '#type' => 'fieldset',
      '#title' => t('Views'),
      '#collapsible' => FALSE,
      '#prefix' => '<div id="backoffice-view-wrapper">',
      '#suffix' => '</div>',
    );

    for ($i = 0; $i < $submodule_views; $i++) {
      $form['submodule'][$ix_submodules]['view'][$i] = array(
        '#type' => 'item',
      );

      $form['submodule'][$ix_submodules]['view'][$i]['view_id'] = array(
        '#type' => 'hidden',
        '#title' => t('View id'),
        '#required' => FALSE,
        '#value' => '',
      );

      $form['submodule'][$ix_submodules]['view'][$i]['view_alias'] = array(
        '#type' => 'textfield',
        '#title' => t('View alias'),
        '#description' => t('Overwrite the title of the views\' display.'),
        '#required' => FALSE,
      );

      $views_objects = views_get_enabled_views();
      $views = array(0 => 'Please select a view');
      foreach ($views_objects as $view) {
        foreach ((array)$view->display as $name => $display) {
          if ($name != 'default') {
            $views[$view->human_name][$view->name . '||' . $name] = $display->display_title;
          }
        }
      }

      $form['submodule'][$ix_submodules]['view'][$i]['view'] = array(
        '#type' => 'select',
        '#title' => t('View'),
        '#options' => $views,
        '#default_value' => 0,
        '#description' => t('Add a view to the submodule.'),
      );
    }

    $form['submodule'][$ix_submodules]['view']['repeater'] = array(
      '#type' => 'submit',
      '#value' => t('Add another view'),
      '#submit' => array('backoffice_repeat_view'),
      '#ajax' => array(
        'callback' => 'backoffice_repeat_view_callback',
        'wrapper' => 'backoffice-view-wrapper',
        'progress' => array('type' => 'none'),
      ),
    );

    $form['submodule'][$ix_submodules]['add_link'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create add link.'),
    );
  }

  $form['submodule']['repeater'] = array(
    '#type' => 'submit',
    '#value' => t('Add another submodule'),
    '#submit' => array('backoffice_repeat_submodule'),
    '#ajax' => array(
      'callback' => 'backoffice_repeat_submodule_callback',
      'wrapper' => 'backoffice-submodule-wrapper',
      'progress' => array('type' => 'none'),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $form;
}

function backoffice_repeat_submodule_callback($form, $form_state) {
  return $form['submodule'];
}

function backoffice_repeat_view_callback($form, $form_state) {
  return $form['submodule'][$form_state['clicked_button']['#array_parents'][1]]['view'];
}

function backoffice_repeat_submodule($form, &$form_state) {
  $form_state['form_structure'][max(array_keys($form_state['form_structure'])) + 1] = 1;
  $form_state['rebuild'] = TRUE;
}

function backoffice_repeat_view($form, &$form_state) {
  $form_state['form_structure'][$form_state['clicked_button']['#array_parents'][1] + 1]++;
  $form_state['rebuild'] = TRUE;
}

function backoffice_admin_modules_form_submit($form, &$form_state) {
  $mid = isset($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : NULL;

  // module
  $module = new stdClass();
  $module->title = $form_state['values']['module_title'];
  $module->icon = $form_state['values']['module_icon'];
  if (!is_null($mid)) {
    $module->mid = $mid;
  }
  drupal_write_record('backoffice_module', $module, $mid ? 'mid' : array());
  $mid = $module->mid;
  // upload icon to public://backoffice as mid.ext
  $icon_path = 'public://backoffice/';
  file_prepare_directory($icon_path, FILE_CREATE_DIRECTORY);
  $icon = file_save_upload('module_icon', array('file_validate_extensions' => array()), $icon_path, FILE_EXISTS_RENAME);
  if ($icon) {
    $icon_extension = drupal_substr($icon->filename, strrpos($icon->filename, '.') + 1);
    $icon = file_move($icon, 'public://backoffice/' . $mid . '.' . $icon_extension, FILE_EXISTS_REPLACE);

    // save icon path
    $module->icon = $icon->fid;
    drupal_write_record('backoffice_module', $module, 'mid');
  }

  // submodule
  //TODO foreach
  $submodule = new stdClass();
  if ($form_state['values']['submodule'][0]['smid']) {
    $submodule->smid = $form_state['values']['smid'];
  }
  $submodule->mid = (int)$mid;
  if ($form_state['values']['submodule'][0]['item']) {
    list($item_type, $item_id) = explode('||', $form_state['values']['submodule'][0]['item']);
  }
  else {
    $item_type = NULL;
    $item_id = NULL;
  }
  switch ($item_type) {
    case 'content_type':
      $item_title = node_type_load($item_id)->name;
      break;
    case 'taxonomy':
      //TODO get tax title
      break;
  }
  $submodule->title = $item_title;
  $submodule->item_type = $item_type;
  $submodule->item_id = $item_id;
  if (!strlen($form_state['values']['submodule'][0]['submodule_alias'])) {
    $submodule->title = $form_state['values']['submodule'][0]['submodule_alias'];
  }
  $submodule->add_link = (int)$form_state['values']['submodule'][0]['add_link'];
  drupal_write_record('backoffice_submodule', $submodule, isset($submodule->smid) ? 'smid' : array());

  // view
  //TODO foreach
  $submodule_view = new stdClass();
  if ($form_state['values']['submodule'][0]['view'][0]['view_id']) {
    $submodule_view->id = $form_state['values']['submodule'][0]['view'][0]['view_id'];
  }
  $submodule_view->smid = $submodule->smid;
  $submodule_view->weight = 0;
  list($submodule_view->view_name, $submodule_view->display_id) = explode('||', $form_state['values']['submodule'][0]['view'][0]['view']);
  $submodule_view->title = views_get_view($submodule_view->view_name)->display[$submodule_view->display_id]->display_title;
  if ($form_state['values']['submodule'][0]['view'][0]['view_alias']) {
    $submodule_view->title = $form_state['values']['submodule'][0]['view'][0]['view_alias'];
  }
  drupal_write_record('backoffice_submodule_views', $submodule_view, isset($submodule_view->id) ? 'id' : array());

  return;
}
